from __future__ import annotations

from abc    import ABCMeta
from copy   import copy
from typing import Optional, Type, Callable, MutableMapping, Any

from toolbelt.core.collections import Stack

from .namespace import Namespace

def prepare_for(
  namespace: Type[ContextualNamespace]
) -> Callable[[type, str, Tuple[object, ...]], ContextualNamespace]:
  @classmethod
  def __prepare__(mcls, name, bases) -> namespace:
    return namespace()

  return __prepare__

def namespace(
  default_factory: DefaultNamespaceFactory      = None,
  processor:       Optional[NamespaceProcessor] = None
):
  return NamespaceGateProvider(default_factory = default_factory, processor = processor)


class NamespaceGate:
  def __init__(
    self,
    namespace:            MutableMapping,
    contextual_namespace: ContextualNamespace,
    namespace_processor:  Optional[NamespaceProcessor] = None
  ):
    self.namespace             = namespace
    self._namespace_processor  = namespace_processor
    self._contextual_namespace = contextual_namespace

  def process_namespace(self) -> Any:
    return (self._namespace_processor(self._contextual_namespace, self.namespace)
            if   self._namespace_processor
            else copy(self.namespace))

  def __enter__(self):
    self._contextual_namespace.push_namespace(self.namespace)

  def __exit__(self, exc_type, exc_value, traceback):
    self._contextual_namespace.pop_namespace()

class NamespaceGateProvider:
  def __init__(
    self,
    *,
    default_factory: DefaultNamespaceFactory      = None,
    processor:       Optional[NamespaceProcessor] = None
  ):
    self._processor       = processor
    self._default_factory = default_factory or DefaultNamespaceFactory()

  def processor(
    self,
    processor: NamespaceProcessor
  ) -> NamespaceGateProvider:
    return type(self)(default_factory = self._default_factory, processor = processor)

  def __set_name__(
    self,
    owner: Type[ContextualNamespace],
    name:  str
  ):
    self._name = name
    owner.register_gate(name)

  def __get__(
    self,
    obj:  ContextualNamespace,
    type: Type[ContextualNamespace] = None
  ):
    if obj is None: return self

    gate = NamespaceGate(
      contextual_namespace = obj,
      namespace_processor  = self._processor,
      namespace            = self._default_factory(obj)
    )

    vars(obj)[self._name] = gate

    return gate

class ContextualNamespace(Namespace):
  GATES_KEY = '_gates_'

  gate_names = None

  @classmethod
  def register_gate(cls, gate_name: str):
    if cls is ContextualNamespace:
      raise TypeError(f"Cannot register gate on `{cls.__name__}`")

    if cls.gate_names is None: cls.gate_names = []

    cls.gate_names.append(gate_name)

  def __init__(self):
    super().__init__()

    self._gates           = None
    self._namespace_stack = Stack[MutableMapping](initials = [{}])

  @property
  def gates(self) -> Dict[str, NamespaceGate]:
    if self._gates is None:
      self._gates = {
        name: namespace_gate
        for name in self.gate_names
        if  isinstance(namespace_gate := getattr(self, name, None), NamespaceGate)
      }

    return self._gates

  @property
  def current_namespace(self) -> MutableMapping:
    return self._namespace_stack.head

  def gate_for_key(self, key) -> Optional[NamespaceGate]:
    return self.gates.get(key)

  def push_namespace(self, namespace: MutableMapping):
    self._namespace_stack.push(namespace)

  def pop_namespace(self):
    self._namespace_stack.pop()

  def __getitem__(self, key):
    # the value of `__annotations__` is set:
    # - conditionally if the body contains some annotation
    # - at the same time as `__module__` and `__qualname__`, right before body is processed
    if key == '__annotations__' and not self._annotations_defined:
      self.current_namespace['__annotations__'] = {}

    if   key == self.GATES_KEY:                        return self.gates
    if   key in self.current_namespace:                return self.current_namespace[key]
    elif (gate := self.gate_for_key(key)) is not None: return gate

    raise KeyError(key)

  def __setitem__(self, key, value): self.current_namespace[key] = value
  def __delitem__(self, key):        del self.current_namespace[key]
  def __len__(self):                 return len(self.current_namespace)
  def __iter__(self):                return iter(self.current_namespace)

  @property
  def _annotations_defined(self):
    return '__annotations__' in self.current_namespace

class DefaultNamespaceFactory(Callable[[ContextualNamespace], MutableMapping]):
  # TODO is the next line correct?
  # def __call__(self, contextual_namespace_self: ContextualNamespace) -> MutableMapping:
  def __call__(self, contextual_namespace_self):
    return {}

NamespaceProcessor = Callable[[ContextualNamespace, MutableMapping], Any]

class NamespaceProcessingProvider(type):
  @classmethod
  def process_namespaces(mcls, namespace: ContextualNamespace):
    return { gate_name: gate.process_namespace()
             for gate_name, gate
             in  namespace.gates.items() }
