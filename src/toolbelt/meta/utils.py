def will_be_direct_instance_of(mcls, bases):
  return not any(b for b in bases if isinstance(b, mcls))
