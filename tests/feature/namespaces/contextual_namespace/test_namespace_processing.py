import pytest

from toolbelt.meta.namespaces import (
  ContextualNamespace, NamespaceProcessingProvider, namespace, prepare_for
)

class CharacterSheetNamespace(ContextualNamespace):
  attributes = namespace()

  @attributes.processor
  def attributes(self, namespace):
    return list(namespace.items())

class CharacterSheetType(NamespaceProcessingProvider):
  __prepare__ = prepare_for(CharacterSheetNamespace)

  def __new__(mcls, name, bases, namespace: CharacterSheetNamespace):
    processing_result = mcls.process_namespaces(namespace)

    return super().__new__(mcls, name, bases, { 'processing_result': processing_result })

class CharacterSheet(metaclass = CharacterSheetType): pass


def test_namespace_processing():
  class TestedCharacter(CharacterSheet):
    name = 'Jared'

    with attributes:
      dexterity = 6
      strength  = 10

  actual   = TestedCharacter.processing_result
  expected = { 'attributes': [('dexterity', 6), ('strength', 10)] }

  assert actual == expected
