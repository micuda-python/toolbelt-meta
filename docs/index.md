# toolbelt-meta

For documentation see tests: [https://gitlab.com/micuda-python/toolbelt-meta/-/tree/master/tests](https://gitlab.com/micuda-python/toolbelt-meta/-/tree/master/tests)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/micuda-python/toolbelt-meta/-/issues](https://gitlab.com/micuda-python/toolbelt-meta/-/issues)

## Requirements

Install requirements for setup beforehand using

```bash
poetry install -E test
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

This project is licensed under the terms of the MIT license.
