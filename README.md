# toolbelt-meta
[![pipeline status](https://gitlab.com/micuda-python/toolbelt-meta/badges/master/pipeline.svg)](https://gitlab.com/micuda-python/toolbelt-meta/-/commits/master)
[![coverage report](https://gitlab.com/micuda-python/toolbelt-meta/badges/master/coverage.svg)](https://gitlab.com/micuda-python/toolbelt-meta/-/commits/master)

*Collection of meta-programming related utils*

For documentation see tests: [https://gitlab.com/micuda-python/toolbelt-meta/-/tree/master/tests](https://gitlab.com/micuda-python/toolbelt-meta/-/tree/master/tests)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/micuda-python/toolbelt-meta/-/issues](https://gitlab.com/micuda-python/toolbelt-meta/-/issues)

## Requirements for builds

Install requirements for setup beforehand using

```bash
poetry install -E test -E build
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

Distributed under the MIT License. See `LICENSE` for more information.
